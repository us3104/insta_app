Rails.application.routes.draw do
  
  devise_for :users, controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }

  root   'static_pages#home'
  get    '/help',    to: 'static_pages#help'
  get    '/about',   to: 'static_pages#about'
  get    '/contact', to: 'static_pages#contact'
  get    'password_resets/new'
  get    'password_resets/edit'
  get    '/signup',  to: 'users#new'
  get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
  get    'likes/create'
  get    'likes/destroy'
  get    '/auth/:provider/callback',    to: 'users#facebook_login',      as: :auth_callback
  get    '/auth/failure',               to: 'users#auth_failure',        as: :auth_failure

  resources :users
  resources :account_activations, only: :edit
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :microposts,          only: [:index, :create, :destroy, :show]
  resources :likes,               only: [:create, :destroy]
  resources :relationships,       only: [:create, :destroy]
  resources :notifications,       only: [:index]
  
  resources :users do
    member do
      # users/:id/get以下
      get :following, :followers
    end
  end
  
  resources :microposts do
    resources :comments, only: [:create, :destroy]
  end
end