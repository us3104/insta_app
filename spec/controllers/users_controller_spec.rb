require 'rails_helper'

RSpec.describe UsersController, type: :controller do
  
  describe "GET #new" do
    it "should be success" do
      get :new
      expect(response).to have_http_status(:success)
    end
  end
  
  describe "return error message" do
    it "should return error message" do
      user = User.new
      user.valid?
      expect(user.errors.messages[:name]).to include("can't be blank")
    end
  end

end
