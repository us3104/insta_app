require 'rails_helper'

RSpec.describe User, type: :model do
  before do
    @user = FactoryBot.build(:user)
  end
  
  context "when user is valid" do
    it "fill user value" do
      expect(@user).to be_valid
    end
    it "email empty" do
      @user.email = " "
      expect(@user).to be_invalid
    end
  end
  
  it "is invalid email format" do
    emails = %w[user_at_foo.org example.user@foo.foo@bar_baz.com foo@bar+baz.com]
    emails.each do |email|
      expect(FactoryBot.build(:user, email: email)).to be_invalid
    end
  end
  
  it "is email should be uniqueness" do
    dup_user = @user.dup
    @user.save
    expect(dup_user).to be_invalid
  end
  
  it "is email case_sensitive is false" do
    @user.email = "Foo@ExAMPle.CoM"
    @user.save!
    expect(@user.reload.email).to eq "foo@example.com"
  end
  
  it "is password should be valid" do
    @user.password = " "
    @user.save
    expect(@user).to be_invalid
  end
  
  describe "password length" do
    context "length is over 6 charactor" do
      it "is password should be valid" do
        @user.password = "s" * 6
        @user.save!
        expect(@user).to be_valid
      end
    end
    context " password length is below 6 charactor" do
      it "is password should be invalid" do
        @user.password = "s" * 5
        @user.save
        expect(@user).to be_invalid
      end
    end
  end
end