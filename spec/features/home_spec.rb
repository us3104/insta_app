require 'rails_helper'

RSpec.feature "Home", type: :feature do

  it "move to term of service" do
    visit root_path
    click_link "Terms of service"
    expect(page).to have_content("利用規約")
  end

  it "move to term of contact" do
    visit root_path
    click_link "Contact"
    expect(page).to have_content("Contact the Ruby on Rails Tutorial")
  end

  it "move to Sign up" do
    visit root_path
    click_on "Sign up now!"
    expect(page).to have_content("Name")
  end

end