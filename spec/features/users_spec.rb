require 'rails_helper'

RSpec.feature "Users", type: :feature do
  
  scenario "users can registration" do
    user = FactoryBot.build(:user,name: "test",email: "test@example.org", password: "abcdefg", password_confirmation: "abcdefg")
    users = User.all
    visit new_user_path

    expect {
    fill_in "Name", with: user.name
    fill_in "Email", with: user.email
    fill_in "Password", with: user.password
    fill_in "Confirmation", with: user.password_confirmation
    click_button "Create my account"
    }.to change{users.count}.from(34).to(35)
  end
  
  scenario "エラーメッセージが正しく表示されること" do
    visit new_user_path

    fill_in "Name", with: " "
    fill_in "Email", with: " "
    fill_in "Password", with: " "
    fill_in "Confirmation", with: " "
    click_button "Create my account"

    expect(page).to have_content("can't be blank")

  end
end
