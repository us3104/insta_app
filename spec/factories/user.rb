FactoryBot.define do
  factory :user do
    id { "999" }
    user_name { "testusername" }
    name { "testuser1" }
    email { "testuser@email.com" }
    password { "password" }
  end
end