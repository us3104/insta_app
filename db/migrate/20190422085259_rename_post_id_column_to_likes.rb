class RenamePostIdColumnToLikes < ActiveRecord::Migration[5.1]
  def change
     rename_column :likes, :post_id, :micropost_id
  end
end
