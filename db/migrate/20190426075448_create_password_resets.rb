class CreatePasswordResets < ActiveRecord::Migration[5.1]
  def change
    create_table :password_resets do |t|
      t.string :current_password
      t.string :new_password
      t.string :new_password_confirmation

      t.timestamps
    end
  end
end
