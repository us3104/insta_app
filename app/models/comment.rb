class Comment < ApplicationRecord
  has_many :notifications, dependent: :destroy
  belongs_to :user
  belongs_to :micropost
  validates :body, presence: true, length: { maximum: 40 } 
  validates :user_id, presence: true
  validates :micropost_id, presence: true
  
  def commented_notification_by(current_user)
    notification = current_user.active_notifications.new(
      comment_id: self.id,
      micropost_id: self.micropost_id,
      visited_id: self.micropost.user_id,
      action: "comment"
      )
    notification.save if notification.valid?
  end
end
