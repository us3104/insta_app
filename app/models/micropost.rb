class Micropost < ApplicationRecord
  has_many :likes, dependent: :destroy
  has_many :favorite_users, through: :likes, source: :user
  has_many :comments, dependent: :destroy
  has_many :notifications, dependent: :destroy
  belongs_to :user
  default_scope -> { order(created_at: :desc) }
  mount_uploader :picture, PictureUploader
  validates :user_id, presence: true
  validates :content, presence: true, length: { maximum: 140 }
  validate  :picture_size
  
  def favorite(user)
    likes.create(user_id: user.id)
  end
  
  def unfavorite(user)
    likes.find_by(user_id: user.id).destroy
  end
  
  def favorite?(user)
    favorite_users.include?(user)
  end
  
  def self.search(search) 
    if search
      where(['content LIKE ?', "%#{search}%"]) 
    else
      all 
    end
  end
  
  def liked_notification_by(current_user)
    notification = current_user.active_notifications.new(
      micropost_id: self.id,
      visited_id: self.user_id,
      action: "like"
    )
    notification.save if notification.valid?
  end
  
  private

    # アップロードされた画像のサイズをバリデーションする
    def picture_size
      if picture.size > 5.megabytes
        errors.add(:picture, "should be less than 5MB")
      end
    end
end