module NotificationsHelper
  @comment = nil
  def notification_form(notification)
    visiter=link_to notification.visiter.name, notification.visiter, style:"font-weight: bold;"
    your_post=link_to 'your_post', notification.micropost, style:"font-weight: bold;"
    case notification.action
      # when "follow" then
        # "#{visiter}followed you"
      when "like" then
        "#{visiter} liked #{your_post}."
      when "comment" then
        "#{visiter} commented #{your_post}."
    end
  end
  
  def unchecked_notifications?
    @notifications = current_user.passive_notifications.where(checked: false) 
    @notifications.nil?
  end 
end